package sistemaVotacionTP;

/*IREP:
 * Tipo de mesa de un Votante:
 *		*"Trabajador" si trabajador==true
 *		*"Enf_Preex" o "Mayor65" si edad>65, efermedad==true y trabajador == false
 *		*"Mayor65" si edad>65, efermedad==false y trabajador == false
 *		*"Enf_Preex" si efermedad==true, edad<=65 y trabajador == false
 *		*"General" si efermedad==false, edad<=65 y trabajador == false
 *
 * Un votante debe tener 16 a�os o m�s (edad > 15)	
 *
 * Un Votante no puede haber votado (voto==true) y no tener turno asignado (turnoAsignado==false y 
 * turno == null)
 * Asimismo, un Votante no puede ser presidente (esPresidente==ture) y no tener turno asignado
 * Un Votante presidente debe tener un tipo de mesa igual a la mesa de la que sea presidente, sin
 * importar sus atributos
 * 
 * Dos tipo Votante tiene una identificaci�n �nica. Se diferencia uno de otro con el n�mero de DNI (dni),
 * por lo que si dos tipo Votante tiene el mismo DNI se los considera el mismo Votante
 * 
 * El turno, represantado por una Tupla de Integer, debe estar conformado por el n�mero de mesa en la 
 * que vota el Votante y la franja horaria en la que vota (siendo las franjas v�lidas las 
 * siguientes diez: 8, 9, 10, 11, 12, 13, 14, 15, 16, 17)
 * */
	

public class Votante {
	private String nombre;
	private int dni;
	private int edad;
	private boolean turnoAsignado;
	private boolean voto;
	private boolean esPresidente;
	private boolean enfermedad;
	private boolean trabajador;
	private String tipoMesa;
	private Tupla<Integer, Integer> turno;
	

	public Votante(int nroDni
				, String nombreVot
				, int edadVot
				, boolean enfPrevia
				, boolean trabaja) {
		
		dni = nroDni;
		nombre = nombreVot;
		edad = edadVot;
		enfermedad = enfPrevia;
		trabajador = trabaja;
		turnoAsignado = false;
		voto = false;

		if(trabajador)
			tipoMesa = "Trabajador";
		else if(edad>65 && enfermedad)
			//debe asignar segun que mesa haya disponible: mayor o enfermo
			tipoMesa = "Mayor65/Enf_Preex";
		else if(enfermedad)
			tipoMesa = "Enf_Preex";
		else if(edad>65)
			tipoMesa = "Mayor65";
		else 
			tipoMesa = "General"; 
	}				

	public String getTipoMesa() {
		return tipoMesa;
	}

	public void setTipoMesa(String tipoMesa) {
		this.tipoMesa = tipoMesa;
	}

	public boolean isTurnoAsignado() {
		return turnoAsignado;
	}

	public void setTurnoAsignado() {
		this.turnoAsignado = true;
	}

	public Tupla<Integer, Integer> getTurno() {
		return turno;
	}

	public void setTurno(Tupla<Integer, Integer> turno) {
		this.turno = turno;
	}

	public boolean isVoto() {
		return voto;
	}

	public void setVoto() {
		this.voto = true;
	}

	public boolean isEsPresidente() {
		return esPresidente;
	}

	public void setEsPresidente(boolean esPresidente) {
		this.esPresidente = esPresidente;
	}

	public String getNombre() {
		return nombre;
	}

	public int getDni() {
		return dni;
	}

	public boolean isEnfermedad() {
		return enfermedad;
	}

	public boolean isTrabajador() {
		return trabajador;
	}

	public boolean equals(Votante v) {
		if(v == null)
			return false;
		return this.dni == v.getDni();
	}
}
