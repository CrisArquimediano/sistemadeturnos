package sistemaVotacionTP;

import java.util.ArrayList;

/*IREP:
 * Como subclase de Mesa, mantiene su invariante de representaci�n.
 * Adem�s, se debe cumplir que tenga una sola franja horaria (8 a 12, indicada con 8) sin cupo.
 * Solo acepta votantes con edad>15 que sean trabajadores (con certificado, representado por un booleano)
 * y un presidente de mesa �nico (de cualquier caracter�stica, siempre y cuando est� registrado). 
 * */

public class Trabajador extends Mesa {

	Trabajador(int dni) {
		super(dni);
		asignadosXfranja.put(8, new ArrayList<Integer>());
	}

	@Override
	public Tupla<Integer, Integer> asignarTurno(Votante v) {
		Tupla<Integer, Integer> turno = new Tupla<>(this.getNroDeMesa(), 8);
		v.setTurno(turno);
		this.agregarVotantesEnMesa(v);
		v.setTurnoAsignado();
		asignadosXfranja.get(8).add(v.getDni());
		return turno;
	}
}
