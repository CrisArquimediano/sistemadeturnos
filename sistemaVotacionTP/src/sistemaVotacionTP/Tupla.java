package sistemaVotacionTP;

//IREP: true.
public class Tupla<T1, T2> {

	private T1 x;
	private T2 y; 

	public Tupla (T1 primero, T2 segundo) {
		x = primero;
		y = segundo;
	}

	public T1 getX() {
		return this.x;
	}

	public T2 getY() {
		return this.y;
	}

	public void setX(T1 primero) {
		this.x = primero;
	}

	public void setY(T2 segundo) {
		this.y = segundo;
	}

	@Override
	public String toString() {
		return "Tupla [x=" + x + ", y=" + y + "]";
	}
	
}
