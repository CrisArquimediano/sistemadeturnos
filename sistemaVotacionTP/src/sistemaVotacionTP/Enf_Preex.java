package sistemaVotacionTP;

/*IREP:
 * En tanto es una subclase de MesaConCupo, se le aplica su mismo invariante de representaci�n.
 * Adem�s se debe cumplir lo siguiente:
 		*Los votantes asignados a esta mesa deben tener 16 o m�s a�os. Exceptuando el presidente, 
 		*todos los votantes deben cumplir que tienen enfermedad preexistente, pueden ser mayores
 		*de 65, pero no pueden trabajar ese d�a;
 		*Debe tener diez franjas horarias (8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
 		*Cada franja horaria debe tener entre 0 y 20 cupos (teniendo 20 al inicio y 0 al quedarse
 		*sin turnos disponibles);
 		* El cupo total debe estar entre 0 y 200 (200 al crearse, 0 al no tener turnos disponibles).
 * */

public class Enf_Preex extends MesaConCupo{

	Enf_Preex(int dni) {
		super(dni);
		this.setCupoTotal(200);
		this.definirCupoYFranja(20);
	}
}
