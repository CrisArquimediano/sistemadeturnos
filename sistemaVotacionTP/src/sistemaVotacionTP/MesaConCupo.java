package sistemaVotacionTP;

import java.util.ArrayList;


/*IREP:
 * Una MesaConCupo es una subclase de Mesa, por lo que se le aplica el invariante de representaci�n de
 * Mesa. Aun as� debe cumplirse:
 		* El cupo total de una MesaConCupo sea mayor o igual cero, indicando este �ltimo caso
 		* que ya no puede asignar m�s turnos (el turno que "asigna" es null).
 		* La lista de tuplas horarios, contiene dos naturales que indican la franja horaria
 		* y el cupo de dicha franja horaria. La suma de todos los cupos de las franjas horarias
 		* debe ser siempre menor o igual al cupo total. Adem�s, cuando el cupo de una franja horaria
 		* llega a cero, no se pueden asignar m�s turnos en dicha franja horaria y se debe asignar en
 		* otra que tenga turno disponible. En caso de que ninguna franja horaria tenga disponibilidad
 		* se debe asignar un turno nulo.
 * */

public abstract class MesaConCupo extends Mesa implements Comparable<MesaConCupo> {
	
	private int cupoTotal; 
	private ArrayList<Tupla<Integer, Integer>> horarios;
	//Tupla: franjas horarias-cupo disponible
	

	MesaConCupo(int dni) {		
		super(dni);
		horarios = new ArrayList<>();
	}
	
	protected void definirCupoYFranja(int cupo) {
		for(int i = 8; i < 18; i++) {
			horarios.add(new Tupla<Integer, Integer>(i, cupo));
			asignadosXfranja.put(i, new ArrayList<Integer>());
		}
	}
	
	public Tupla<Integer, Integer> asignarTurno(Votante v){ 
		
		Tupla<Integer, Integer> turno = null;

		if(cupoTotal <= 0) {
			return turno;
		}
		else {
			for(Tupla<Integer, Integer> h: horarios) {
				if(h.getY() > 0) {
					h.setY(h.getY()-1);

					turno = new Tupla<>(this.getNroDeMesa(), h.getX());
					v.setTurno(turno);
					this.agregarVotantesEnMesa(v);
					v.setTurnoAsignado();
					asignadosXfranja.get(h.getX()).add(v.getDni());
					cupoTotal--;
					return turno;
				}
			}
			return turno;
		}		
	}
	
	@Override
	/*para poder utilizar TreeSet, con orden de menor a mayor, tal que 
	 *al pedir la �ltima mesa siempre de la de mayor cupo disponible*/
	public int compareTo(MesaConCupo otra) {
		return this.cupoTotal-otra.cupoTotal;
	}

	protected void setCupoTotal(int cupoTotal) {
		this.cupoTotal = cupoTotal;
	}

	public ArrayList<Tupla<Integer, Integer>> getHorarios() {
		return horarios;
	}
	
}