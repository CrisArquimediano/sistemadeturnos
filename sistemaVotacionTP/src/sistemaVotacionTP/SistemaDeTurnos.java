package sistemaVotacionTP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/*IREP:
 * El Sistema debe tener nombre, no puede ser null.
 * 
 * Los votantes se guardan a medida que se van registrando y se conservan a lo largo de la vida del
 * programa. No puede haber votantes repetidos ni votantes menores de 16 a�os.
 * 
 * Hay solo cuatro tipos de mesas ("Mayor65", "Enf_Preex", "Trabajador", "General). Se tiene un registro
 * de cada mesa creada (cada mesa se puede crear una sola vez).
 * 
 * Al agregar una mesa nueva, autom�ticamente se debe asignar un presidente y un turno para dicho
 * presidente en la mesa en cuesti�n (es decir, no puede existir una mesa sin presidente ni un
 * presidente sin turno o con turno en otra mesa).
 * */

public class SistemaDeTurnos {
	
	private String sistema;
	private Map<Integer, Votante> votantes;
	private Map<Integer, Mesa> mesas;
	private TreeSet<Mayor65> mesasMayorDisp;
	private TreeSet<Enf_Preex> mesasEnfermoDisp;
	private Trabajador mesaTrabajador; //tiene cupos ilimitados, una es suficiente
	private TreeSet<General> mesasGeneralDisp;
	
	
	SistemaDeTurnos(String nombreSistema){
		if(nombreSistema == null)
			throw new RuntimeException("El sistema debe tener un nombre.");
		sistema = nombreSistema;
		votantes = new HashMap<>();
		mesas = new HashMap<>();
		mesasMayorDisp = new TreeSet<>();
		mesasEnfermoDisp = new TreeSet<>();
		mesasGeneralDisp = new TreeSet<>();		
	}
	
	
	public void registrarVotante(int dni, String nombre, int edad, boolean enfPrevia, boolean trabaja) {
		if(edad < 16)
			throw new RuntimeException("Menores de 16 a�os no votan.");
		Votante votante = new Votante(dni, nombre, edad, enfPrevia, trabaja);
		votantes.put(dni, votante);
	}
	
	
	public int agregarMesa(String tipoMesa, int dni) {
		
		if(!votantes.containsKey(dni))
			throw new RuntimeException("La persona con DNI: "+dni+", debe estar registrada para poder ser presidente de mesa.");
		
		if(votantes.get(dni).isEsPresidente())
			throw new RuntimeException("La persona con DNI: "+dni+", ya es presidente de otra mesa.");
		
		if(mesaValida(tipoMesa)) 
			return crearMesa(tipoMesa, dni);
		else 
			throw new RuntimeException("El tipo de mesa '"+tipoMesa+"' no es v�lido.");		 
	}
	

	public Tupla <Integer, Integer> asignarTurno(int dni){

		if(!votantes.containsKey(dni))
			throw new RuntimeException("La persona con DNI: "+dni+", no se encuentra registrada en el sistema.");
		else {
			Votante votante = votantes.get(dni);

			if(votante.isTurnoAsignado())
				return votante.getTurno();

			else {
				try {
					return darMesa(votante.getTipoMesa()).asignarTurno(votante);
				} catch(Exception e) {
					return null;
				}
			}
		}
	}
	


	
	public int asignarTurnos() {
		int contador = 0;
		for(Integer dni: votantes.keySet()) {
			if(!votantes.get(dni).isTurnoAsignado()) {
				asignarTurno(dni);
				contador++;
			}
		}
		return contador;
	}
	
	public boolean votar(int dni) {
		if(!votantes.containsKey(dni))
			throw new RuntimeException("La pesona con DNI: "+dni+", no se encuentra registrada en el sistema.");
		Votante v = votantes.get(dni);
		if(v.isVoto()) 
			return false;
		else {
			v.setVoto();
			return v.isVoto();
		}
	}
	
	
	public int votantesConTurno(String tipoMesa) { 
		if(!mesaValida(tipoMesa)) 
			throw new RuntimeException("El tipo de mesa '"+tipoMesa+"' es inv�lido.");

		int cantidadVot = 0;
		for(Integer nroMesa: mesas.keySet()) {
			Mesa mesa = mesas.get(nroMesa);

			if(mesa.getClass().getSimpleName().equals(tipoMesa)) {
				cantidadVot += mesa.cantidadVotantesEnMesa();
			}	
		}
		return cantidadVot;
	}
	
	
	public Tupla<Integer, Integer> consultaTurno(int dni){
		if(!votantes.containsKey(dni))
			throw new RuntimeException("La persona con DNI: "+dni+", no est� registrada en el sistema.");
		
		return votantes.get(dni).getTurno();
	}
	

	//Dado que si hay una mesa hay un presidente, siempre habr� al menos un votante asignado
	public Map <Integer, List<Integer>> asignadosAMesa(int numMesa){
		if(!mesas.containsKey(numMesa))
			throw new RuntimeException("El n�mero de mesa: "+numMesa+", no es v�lido.");

		return mesas.get(numMesa).getAsignadosXfranja();		
	}
	
	
	public List<Tupla<String, Integer>> sinTurnoSegunTipoMesa(){
		List<Tupla<String, Integer>> votsSinTurno = new ArrayList<>();

		votsSinTurno.add(new Tupla<>("Trabajador", 0));
		votsSinTurno.add(new Tupla<>("General", 0));
		votsSinTurno.add(new Tupla<>("Mayor65", 0));
		votsSinTurno.add(new Tupla<>("Enf_Preex", 0));


		for(Integer dni: votantes.keySet()) {
			Votante vot = votantes.get(dni);
			if(vot.getTipoMesa().equals("Trabajador") && !vot.isTurnoAsignado()) {
				votsSinTurno.get(0).setY(votsSinTurno.get(0).getY()+1);
			}
			else if(vot.getTipoMesa().equals("General") && !vot.isTurnoAsignado()) {
				votsSinTurno.get(1).setY(votsSinTurno.get(1).getY()+1);
			}
			else if(vot.getTipoMesa().equals("Mayor65")  && !vot.isTurnoAsignado()) {
				votsSinTurno.get(2).setY(votsSinTurno.get(2).getY()+1);
			}
			else if(vot.getTipoMesa().equals("Enf_Preex")  && !vot.isTurnoAsignado()) {
				votsSinTurno.get(3).setY(votsSinTurno.get(3).getY()+1);
			}
			else if(vot.getTipoMesa().equals("Mayor65/Enf_Preex") && !vot.isTurnoAsignado()) {
				//vota en la primera que tenga turno, espera por alguna de las dos
				votsSinTurno.get(2).setY(votsSinTurno.get(2).getY()+1);
				votsSinTurno.get(3).setY(votsSinTurno.get(3).getY()+1);
			}
		}
		return votsSinTurno;
	}
	
	
	////////// M�todos Privados //////////
	
	private boolean mesaValida(String tipoMesa) {
		return tipoMesa.equals("Mayor65")   
			|| tipoMesa.equals("Enf_Preex")
			|| tipoMesa.equals("Trabajador")
			|| tipoMesa.equals("General");	
	}
	
	private void darTurnoAlPresidente(String tipoMesa, int dni) {
		votantes.get(dni).setEsPresidente(true);
		votantes.get(dni).setTipoMesa(tipoMesa);
		asignarTurno(dni);
	}

	private int crearMesa(String tipoMesa, int dni) {
		Mesa mesa;
		if(tipoMesa.equals("Mayor65")) {
			mesa = new Mayor65(dni);
			mesasMayorDisp.add((Mayor65) mesa);
		}
		else if(tipoMesa.equals("Enf_Preex")) {
			mesa = new Enf_Preex(dni);
			mesasEnfermoDisp.add((Enf_Preex) mesa);
		}
		else if(tipoMesa.equals("Trabajador")) {
			mesa = new Trabajador(dni);
			mesaTrabajador = (Trabajador) mesa;
		}
		else{
			mesa = new General(dni);
			mesasGeneralDisp.add((General) mesa);
		}
		mesas.put(mesa.getNroDeMesa(), mesa);
		darTurnoAlPresidente(tipoMesa, dni);
		return mesa.getNroDeMesa();
	}
	
	private Mesa darMesa(String tipoMesa) {
		if(tipoMesa.equals("Mayor65/Enf_Preex")) {
			if(!mesasMayorDisp.isEmpty())
				return mesasMayorDisp.last();
			else
				return mesasEnfermoDisp.last();
		}
		else if(tipoMesa.equals("Enf_Preex")) 
			return mesasEnfermoDisp.last();
		else if(tipoMesa.equals("Mayor65"))
			return mesasMayorDisp.last();
		else if(tipoMesa.equals("Trabajador"))
			return mesaTrabajador;	
		else 
			return mesasGeneralDisp.last();

		//si last() da excepci�n, el m�todo que hace la llamada tiene try/catch
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Elecciones: Sistema de Turnos - "+sistema+"\n\n");
		sb.append("Votantes sin turno en espera seg�n tipo de mesa: \n"
					+toStringSinTurno(sinTurnoSegunTipoMesa())+"\n\n");
		sb.append("Votantes con turno: \n" + darVotsConTurnoYmesa()+"\n\n");
		sb.append(darMesasConPresidente());
		
		return sb.toString();
	}
	
	
	private String darMesasConPresidente() {
		StringBuilder mesaPres = new StringBuilder();
		
		for(Integer nroMesa: mesas.keySet()) {
			int dni = mesas.get(nroMesa).getDni_presidente();
			Votante vot = votantes.get(dni);
			String nombrePres = vot.getNombre();
			mesaPres.append("Tipo de mesa: "+mesas.get(nroMesa).getClass().getSimpleName()
					+". - Presidente: "+nombrePres+".\n");
		}
		
		return mesaPres.toString();
	}


	private String darVotsConTurnoYmesa() {
		StringBuilder asignados = new StringBuilder();

		for(Integer dni: votantes.keySet()) {
			if(votantes.get(dni).isTurnoAsignado()) {
				asignados.append(votantes.get(dni).getNombre()+", ");
				asignados.append(toStringTurno(votantes.get(dni).getTurno())+" ");
				if(votantes.get(dni).isVoto())
					asignados.append("Vot�.\n");
				else
					asignados.append("No vot�.\n");
			}
		}
		return asignados.toString();
	}
	
	
	private String toStringTurno(Tupla<Integer, Integer> turno) {
		String nroMesa = turno.getX().toString();
		String hora = turno.getY().toString();
		return "vota en la mesa n�mero "+nroMesa+" a las "+hora+"hs.";
	}
	
	
	private String toStringSinTurno(List<Tupla<String, Integer>> sinTurno) {
		StringBuilder sb = new StringBuilder();
		for(Tupla<String, Integer> t: sinTurno) {
			String tipoMesa = t.getX();
			String cantTurnos = t.getY().toString();
			sb.append("Tipo de mesa: "+tipoMesa+", cantidad de votantes esperando turno: "
						+cantTurnos+".\n");
		}
		return sb.toString();
	}
	
	////////// Getters //////////
	/*Necesarios para desde afuera poder acceder a las mesas, a los votantes y al nombre del sistema*/
	public String getSistema() {
		return sistema;
	}

	public TreeSet<Mayor65> getMesasMayorDisp() {
		return mesasMayorDisp;
	}

	public TreeSet<Enf_Preex> getMesasEnfermoDisp() {
		return mesasEnfermoDisp;
	}

	public Trabajador getMesaTrabajador() {
		return mesaTrabajador;
	}

	public TreeSet<General> getMesasGeneralDisp() {
		return mesasGeneralDisp;
	}
	
	public Map<Integer, Votante> getVotantes() {
		return votantes;
	}

}