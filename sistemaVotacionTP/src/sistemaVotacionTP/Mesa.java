package sistemaVotacionTP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*IREP:
 * Una Mesa es irrepetible y �nica, y se identifica con un n�mero natural.
 * Dos tipo Mesa con igual n�mero identificatorio (mismo nroDeMesa), se consideran la misma Mesa.
 * 
 * Una Mesa debe tener un presidente de mesa que se identifica con un n�mero entero el cual a su
 * vez representa a un Votante. Dicho presidente debe ser, adem�s, un Votante registrado (dada una
 * Mesa en un sistema, no es posible que su presidente sea alguien no registrado en dicho sistema).
 * 
 * La lista votantesEnMesa debe guardar una �nica vez a cada Votante que est� asignado a la Mesa.
 * 
 * El registro de los votantes que tienen turno en una franja horaria ofrecido por la Mesa
 * se guarda en un diccionario que debe cumplir lo siguiente:
 * 			*Debe tener por claves a cuantas franjas horarias permita la Mesa involucrada
 * 			*Dichas claves deben tener por valor una lista de tipo Votante, donde se guarda,
 * 			 sin repetici�n, a cada Votante con turno en dicha franja horaria.
 * 
 * */

public abstract class Mesa {
	private static int contadorMesas = 0;
	private int nroDeMesa;
	private int dni_presidente;
	private ArrayList<Votante> votantesEnMesa;
	protected Map<Integer, List<Integer>> asignadosXfranja;
	//Key: franjaHoraria, Valor: Lista DNI de los asignados 
	
	Mesa(int dni){
		contadorMesas++;
		nroDeMesa = contadorMesas;
		votantesEnMesa = new ArrayList<>();
		asignadosXfranja = new HashMap<>();
		dni_presidente = dni;
	}
	
	public abstract Tupla<Integer, Integer> asignarTurno(Votante v);
	
	public boolean equals(Mesa otra) {
		if(otra == null)
			return false;
		return 
		(this == otra || this.getNroDeMesa() == otra.getNroDeMesa());		
	} 
		
	public int getNroDeMesa() {
		return nroDeMesa;
	}

	public int getDni_presidente() {
		return dni_presidente;
	}

	public ArrayList<Votante> getVotantesEnMesa() {
		return votantesEnMesa;
	}
	
	public void agregarVotantesEnMesa(Votante v) {
		this.votantesEnMesa.add(v);
	}
	
	public int cantidadVotantesEnMesa() {
		return votantesEnMesa.size();
	}
	
	public Map<Integer, List<Integer>> getAsignadosXfranja() {
		return asignadosXfranja;
	}
		
	@Override
	public String toString() {
		return getClass().getSimpleName();
	}
	
}