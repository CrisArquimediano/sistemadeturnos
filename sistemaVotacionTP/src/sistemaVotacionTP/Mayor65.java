package sistemaVotacionTP;

/*IREP:
 * En tanto es una subclase de MesaConCupo, se le aplica su mismo invariante de representaci�n.
 * Adem�s se debe cumplir lo siguiente:
 		*Los votantes asignados a esta mesa deben tener 16 o m�s a�os y, salvo el presidente, 
 		*deben ser mayores de 65 (edad>65), puede tener enfemerdad preexistente, pero no trabajar ese d�a; 
 		*Debe tener diez franjas horarias (8, 9, 10, 11, 12, 13, 14, 15, 16, 17);
 		*Cada franja horaria debe tener entre 0 y 10 cupos (teniendo 10 al inicio y 0 al quedarse
 		*sin turnos disponibles);
 		* El cupo total debe estar entre 0 y 100 (100 al crearse, 0 al no tener turnos disponibles).
 * */

public class Mayor65 extends MesaConCupo{

	Mayor65(int dni) {
		super(dni);
		this.setCupoTotal(100);
		this.definirCupoYFranja(10);
		
	}
}
