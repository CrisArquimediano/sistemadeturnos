package sistemaVotacionTP;

public class UsoSistema {


	public static void main(String[] args) {
		SistemaDeTurnos s = new SistemaDeTurnos("Votaciones Generales, Ucrania");
		s.registrarVotante(40, "Pepe", 20, false, false);
		s.registrarVotante(4020, "Pipo", 70, false, false);
		s.registrarVotante(41, "Pe", 56, true, false);
		s.registrarVotante(42, "Guy", 38, false, true);
		
		s.registrarVotante(1, "Jorge", 38, false, true);
		s.registrarVotante(2, "Juan Carlos", 38, false, true);
		s.registrarVotante(3, "Marta", 38, false, true);
		s.registrarVotante(4, "Beatriz", 38, false, true);
		s.registrarVotante(5, "Jos�", 38, false, true);
		s.registrarVotante(4222, "Ra�l", 38, false, false);
		s.registrarVotante(4233, "Ver�nica", 38, false, false);
		s.registrarVotante(4244, "Matilde", 38, false, false);
		s.registrarVotante(4215, "Blanca", 38, false, false);
		s.registrarVotante(6, "Roxana", 88, false, true);
		s.registrarVotante(7, "Miguel", 38, true, true);
		s.registrarVotante(8, "Marcos", 67, true, true);
		s.registrarVotante(42481, "Jaime", 38, true, false);
		
	
		s.agregarMesa("Mayor65", 42);
		s.agregarMesa("General", 40);	
		s.agregarMesa("Enf_Preex", 41);
		s.agregarMesa("Trabajador", 4020);
		
//		System.out.println("presidente? "+s.getVotantes().get(40).isEsPresidente());
		
//		System.out.println("tiene turno el presidente? "+s.getVotantes().get(4020).isTurnoAsignado());
//		System.out.println("tiene turno el presidente? "+s.getVotantes().get(40).isTurnoAsignado());
//		System.out.println("tiene turno el presidente? "+s.getVotantes().get(41).isTurnoAsignado());
		
		
//		System.out.println("s.asignarTurnos(): "+s.asignarTurnos());
//		System.out.println("tiene turno el votante m1? "+s.getVotantes().get(421).isTurnoAsignado());
		
//		System.out.println("cant de turnos mesa General: "+s.votantesConTurno("General"));
//		System.out.println("cant de turnos mesa Trabajador: "+s.votantesConTurno("Trabajador"));

		s.asignarTurnos();
		
		s.votar(s.getVotantes().get(4020).getDni());
		s.votar(s.getVotantes().get(40).getDni());
		s.votar(s.getVotantes().get(42).getDni());
		s.votar(s.getVotantes().get(41).getDni());
		s.votar(s.getVotantes().get(8).getDni());
		s.votar(s.getVotantes().get(7).getDni());
		s.votar(s.getVotantes().get(1).getDni());
		s.votar(s.getVotantes().get(2).getDni());
		s.votar(s.getVotantes().get(3).getDni());

		
//		System.out.println("Asignados a mesa: "+s.asignadosAMesa(s.getMesaTrabajador().getNroDeMesa()));
		

		
		System.out.println(s.toString());
		
	


	}
}



/*Votante v = new Votante(41, "pe", 56, false, false);
		
		System.out.println("v.isTurnoAsignado(): "+v.isTurnoAsignado()+"\n"
		+"v.isEsPresidente(): "  +v.isEsPresidente()+"\n v.isVoto(): "+v.isVoto());*/