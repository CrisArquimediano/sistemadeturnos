# SistemaDeTurnos

## Descripción
Sistema De Turnos es un programa pensado para organizar una votación de tipo gubernamental. Basado en los sistemas físicos actuales, se implementa la versión virtual. Existen mesas, se registran votantes que cumplan con los requisitos de votación y se les asigna un turno en la mesa que le corresponda. El programa, además de crear las mesas, registrar a los votantes y asignarles turnos, es capaz de reponder consultas puntuales sobre los votantes (mesa donde vota, edad, DNI, etc.) y también información sobre las mesas (tipo, núrmero único, presidente de mesa, etc.).

## Contexto
Se trata de un trabajo final para la materia Programación II de la Universidad Nacional de General Sarmiento en el marco de la Licenciatura en Sistemas.

## Roadmap
Para ampliar este proyecto, se pueden encarar las siguientes ideas:
◙Versión estándar para distintos tipos de votación (que no necesariamente entienden el concepto de "mesa" por ejemplo);
◙Interfaz gráfica;
◙Versión web;
◙Implementación de bases de datos para una utilización en situaciones reales.

## Estado del proyecto
Concluido, con posibilidades de mejores y nuevas implementaciones.

